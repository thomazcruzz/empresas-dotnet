﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2
{
  public class Empresas
  {

    public Empresas()
    {
    }
    /// <summary>
    /// Nome do assembly. Ex: RM.Glb.Api.dll
    /// </summary>
    public string NOME { get; set; }

    /// <summary>
    /// Indica se o assembly foi exportado com sucesso.
    /// 1: Sucesso, 0: Falha, null: Não exportado
    /// </summary>
    public string TIPO { get; set; }
  }
}