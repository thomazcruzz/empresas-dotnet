﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication2.Providers
{
  public class ProviderTokenAccess : OAuthAuthorizationServerProvider
  {
    public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    {

      context.Validated();
    }


    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    {
      var user = Users().FirstOrDefault(x => x.Name == context.UserName
                                        && x.Password == context.Password);

      if (user == null)
      {
        context.SetError("invalid_grant", "Usuário não encontrado ou senha incorreta");
        return;
      }

      var identyUser = new ClaimsIdentity(context.Options.AuthenticationType);
      context.Validated(identyUser);
    }



    public static IEnumerable<User> Users()
    {
      return new List<User>
      {
        new User { Name = "testeapple@ioasys.com.br", Password = "12341234"}
      };
    }
    public class User
    {
      public string Name { get; set; }
      public string Password { get; set; }
    }
  }
}

