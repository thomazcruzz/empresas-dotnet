﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebApplication2;

namespace WebApplication1.Controllers
{
  public class BancoDadosServer : BancoDados
  {
    
    public BancoDadosServer() : base()
    {
      
    }

    public BancoDadosServer(IDbConnection connection) : base(connection)
    {
    }

    public IEnumerable<Empresas> GetEnterprises(string id)
    {
      var lst = _connection.Query<Empresas>(string.Format("SELECT * FROM ENTERPRISES WHERE TIPO = '{0}'", id), commandTimeout: 0);
      return lst;
    }

    public IEnumerable<Empresas> GetEmpresa(string tipo, string nome)
    {
      var lst = _connection.Query<Empresas>(string.Format("SELECT * FROM ENTERPRISES WHERE NOME = '{0}' AND TIPO = '{1}'", nome, tipo), commandTimeout: 0);
      
      return lst;
    }
  }
}