﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebApplication1.Controllers
{
  public class BancoDados : IDisposable
  {
    /// <summary>
    /// Conexão com o banco de dados
    /// </summary>
    protected IDbConnection _connection;   

    static BancoDados()
    {
      Dapper.SqlMapper.AddTypeMap(typeof(string), DbType.AnsiString);
    }

    /// <summary>
    /// Inicialização da classe: <see cref="BancoDadosServer"/>.
    /// </summary>
    /// <param name="connection">Connection</param>
    public BancoDados(IDbConnection connection)
    {
      _connection = connection;
    }

    /// <summary>
    /// Inicialização da classe: <see cref="BancoDadosServer"/>.
    /// </summary>
    public BancoDados()
    { 
      var builder = new System.Data.SqlClient.SqlConnectionStringBuilder()
      {
        DataSource = "BHN050102775\\sql2017",
        InitialCatalog = "baseTest",
        UserID = "teste",
        Password = "thomaz@cruz@123",
        Pooling = false,
        MultipleActiveResultSets = false
      };
      _connection = CreateNew(builder.ToString());
    }

    public void Dispose()
    {
      if (_connection != null)
        _connection.Dispose();
      _connection = null;
    }

    /// <summary>
    /// Cria uma nova conexão com o BD
    /// </summary>
    /// <returns>Conexão com o BD.</returns>
    static IDbConnection CreateNew(string conStr)
    {
      var connection = new System.Data.SqlClient.SqlConnection(conStr);
      try
      {
        connection.Open();
        return connection;
      }
      catch
      {
        connection.Dispose();
        throw;
      }
    }
  }
}