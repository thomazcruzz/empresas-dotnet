﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebApplication2.Models
{
  public class Models
  {

  }

  public class LoginRequest
  {
    public string email { get; set; }
    public string password { get; set; }
  }

  public class LoginResponse
  {
    public LoginResponse()
    {

      this.Token = "";
      this.responseMsg = new HttpResponseMessage() { StatusCode = System.Net.HttpStatusCode.Unauthorized };
    }

    public string Token { get; set; }
    public HttpResponseMessage responseMsg { get; set; }

  }
}