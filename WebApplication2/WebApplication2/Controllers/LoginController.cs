﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
  public class LoginController : ApiController
  {
    [System.Web.Http.HttpPost]
    [System.Web.Http.Route("api/v1/users/auth/sign_in")]
    public IHttpActionResult Authenticate([FromBody] LoginRequest login)
    {
      var loginResponse = new LoginResponse { };
      LoginRequest loginrequest = new LoginRequest { };
      loginrequest.email = login.email.ToLower();
      loginrequest.password = login.password;

      IHttpActionResult response;
      HttpResponseMessage responseMsg = new HttpResponseMessage();
      bool isUsernamePasswordValid = false;

      if (login != null)
        isUsernamePasswordValid = loginrequest.password == "12341234" ? true : false;

      if (isUsernamePasswordValid)
      {
        string token = createToken(loginrequest.email);

        return Ok<string>(token);
      }
      else
      {
        loginResponse.responseMsg.StatusCode = HttpStatusCode.Unauthorized;
        response = ResponseMessage(loginResponse.responseMsg);
        return response;
      }
    }

    private string createToken(string username)
    {

      DateTime issuedAt = DateTime.UtcNow;

      DateTime expires = DateTime.UtcNow.AddDays(7);

      //http://stackoverflow.com/questions/18223868/how-to-encrypt-jwt-security-token
      var tokenHandler = new JwtSecurityTokenHandler();

      ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
      {
                new Claim(ClaimTypes.Name, username)
            });

      const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
      var now = DateTime.UtcNow;
      var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
      var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);

      var token =
          (JwtSecurityToken)
              tokenHandler.CreateJwtSecurityToken(issuer: "http://localhost:50191", audience: "http://localhost:50191",
                  subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
      var tokenString = tokenHandler.WriteToken(token);

      return tokenString;
    }
  }
}