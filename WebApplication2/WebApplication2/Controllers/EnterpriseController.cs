﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2;

namespace WebApplication1.Controllers
{
  [Authorize]
  public class EnterpriseController : ApiController
  {
    [HttpGet]
    [Route("api/v1/enterprises/{Id}")]
    public IHttpActionResult Get(string id)
    {
      using (var server = new BancoDadosServer())
      {

        IEnumerable<Empresas> lst = server.GetEnterprises(id);
        return Json(lst);
      }
    }

    [HttpGet]
    [Route("api/v1/enterprises")]
    public IHttpActionResult Get(string enterprise_types, string name)
    {
      using (var server = new BancoDadosServer())
      {
        IEnumerable<Empresas> lst = server.GetEmpresa(enterprise_types, name);
        return Json(lst);
      }
    }   
  }
}
